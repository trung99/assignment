<%-- 
    Document   : home
    Created on : Jan 7, 2020, 11:40:56 PM
    Author     : a
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${sessionScope.loginUsername == null}">
    <jsp:forward page="login.jsp"/>
    
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
         <link href="css/style.css" type="text/css" rel="stylesheet" />
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <div class="container">            
            <div  class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"><h1 class="home-title">${sessionScope.loginUsername}</h1></div>
        
                <div class="col-md-2">
        <form action="PostServlet" method="POST">
            <input type="hidden" name="username" value="${sessionScope.loginUsername}"/>
            <input type="hidden" name="type" value="all"/>
            <input type="hidden" name="action" value="VIEW"/>
            <input class="home-btn" type="submit" value="Homepage"/>
        </form>
            </div>
         <div class="col-md-2">
        <form action="PostServlet" method="POST">
            <input type="hidden" name="username" value="${sessionScope.loginUsername}"/>
            <input type="hidden" name="type" value="user"/>
            <input type="hidden" name="action" value="VIEW"/>
            <input class="home-btn-1" type="submit" value="My Post"/>
        </form>
            </div>
       
            </div>
            
            <div  class="row margin-top-30">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form action="PostServlet" method="POST">
            <input type="hidden" name="username" value="${sessionScope.loginUsername}"/>
            <input class="cmt-box" type="text" name="content" placeholder="${sessionScope.loginUsername}, what are you thinking?"/><br>
            <input type="hidden" name="action" value="ADD"/>
            <input class="cmt-btn" type="submit" value="Post"/>
        </form>
        
        <c:forEach var="post" items="${getAllPost}">
            <ul style="list-style-type:none;">
                <li style="font-size: 16px;
    font-weight: bold;">${post.username}</li>
                <li style="font-size: 12px !important;
    font-style: italic;">${post.createAt}</li>
                <li>${post.content}</li>
                <li>
                   <input class="cmt-box-1" type="text" name="content" placeholder="Write a comment ..."/><br>
                </li>
                <li>
                    <c:if test="${size == 1}">
                        <jsp:include page="comment.jsp" flush="true"/>
                    </c:if>
                </li>
            </ul>
        </c:forEach>
            </div>
                </div>
            </div>
    </body>
</html>