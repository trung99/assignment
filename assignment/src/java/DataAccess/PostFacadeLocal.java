/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import Entity.Post;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author a
 */
@Local
public interface PostFacadeLocal {

    void create(Post post);

    void edit(Post post);

    void remove(Post post);

    Post find(Object id);

    List<Post> findAll();

    List<Post> findRange(int[] range);

    int count();
    
      
    List<Post> listPost(String username, String type);
    
    Post addPost(String username, String content);
    
}
