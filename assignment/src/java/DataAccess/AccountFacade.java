/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess;

import Entity.Account;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author a
 */
@Stateless
public class AccountFacade extends AbstractFacade<Account> implements AccountFacadeLocal {

    @PersistenceContext(unitName = "Assignment3PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AccountFacade() {
        super(Account.class);
    }

    @Override
    public boolean checkAccount(String username, String password) {
         Query query = em.createNativeQuery("select * from account where username = ? and password = ?", Account.class);
        
        try {
            query.setParameter(1, username);
            query.setParameter(2, password);
            
            return query.getSingleResult() != null;
            
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public boolean register(String username, String password) {
         Query query = em.createNativeQuery("insert into account(username, password) values (?, ?)");
        
        query.setParameter(1, username);
        query.setParameter(2, password);
        
        try {
            int row = query.executeUpdate();
            return row != 0;
            
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public boolean checkUsernameExist(String username) {
       Query query = em.createNativeQuery("select * from account where username = ?", Account.class);
        
        try {
            query.setParameter(1, username);
            
            return query.getSingleResult() != null;
        } catch (Exception e) {
        }
        return false;
    }
    
}
