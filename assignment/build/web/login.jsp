<%-- 
    Document   : login
    Created on : Jan 7, 2020, 11:31:05 PM
    Author     : a
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
         <link href="css/style.css" type="text/css" rel="stylesheet" />
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
       
       
         <div class="container">            
            <div  class="row margin-top-50">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <p class="first-title">ACCOUNT LOGIN</p>
                      <form action="LoginServlet" method="POST">
                          <p class="p-title">USERNAME</p> 
            <input class="input-box" type="text" name="username"/>
            <p class="p-title">PASSWORD</p> 
            <input class="input-box" type="password" name="password">
               <div class="last-title">
                   Create an account? <a href="register.jsp">Register</a> <br>
                     </div>
            <input class="btn-login" type="submit" value="Login"/>
        </form>
                  
      
                </div>
            </div>
        </div>        
    </body>
</html>
